package com.example.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buttonClick = findViewById<Button>(R.id.btnHello)
        buttonClick.setOnClickListener {
            val nameText = findViewById<TextView>(R.id.textViewName)
            val nameMessage = nameText.text.toString()

            val idText = findViewById<TextView>(R.id.textViewId)
            val idMessage = idText.text.toString()

            val intent = Intent(this, HelloActivity::class.java)

            intent.putExtra(EXTRA_MESSAGE, nameMessage);


            startActivity(intent)

            Log.d("Name", nameMessage)
            Log.d("ID",idMessage)
        }


    }


}